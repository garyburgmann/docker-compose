ARG DOCKER_VERSION=20
FROM docker:${DOCKER_VERSION}

ARG VERSION=1.28.0

# don't install docker-compose here
# https://cryptography.io/en/latest/installation.html#alpine
RUN apk add --no-cache \
    py-pip \
    python3-dev \
    libffi-dev \
    openssl-dev \
    gcc \
    libc-dev \
    make \
    bash \
    musl-dev \
    cargo

# Python setup
RUN ln -s /usr/bin/python3 /usr/local/bin/python
RUN ln -s /usr/bin/pip3 /usr/local/bin/pip

RUN pip install --upgrade pip setuptools wheel

# use newer version of docker-compose than apk with --context argument
RUN pip install "docker-compose==${VERSION}"
